var AccountController = function (userModel, session, mailer) {

    this.crypto = require('crypto');
    this.uuid = require('node-uuid');
    this.ApiResponse = require('../models/api-response.js');
    this.ApiMessages = require('../models/api-messages.js');
    this.UserProfileModel = require('../models/user-profile.js');
    this.userModel = userModel;
    this.session = session;
    this.mailer = mailer;
};

AccountController.prototype.getSession = function (){
	return this.session;
};

AccountController.prototype.setSession = function (session){
	this.session = session;
};

AccountController.prototype.hashPassword = function (password, salt, callback) {        
    // we use pbkdf2 to hash and iterate 10k times by default 
    var iterations = 10000,
        keyLen = 64; // 64 bit.
    this.crypto.pbkdf2(password, salt, iterations, keyLen, callback);
};

AccountController.prototype.logon = function (email,password,callback){
	var me = this;
	me.userModel.findOne({email:email}, function(err,user){
		if (err){
			return callback(err, new me.ApiResponse(
				{ success: false, extras: { msg: me.ApiMessages.DB_ERROR } 
			}));
		}

		if (user){
			me.hashPassword(password,user.passwordSalt,function (err,passwordHash){
				if (passwordHash == user.passwordHash){
					var UserProfileModel = new me.UserProfileModel({
						email = user.email,
    					firstName = user.firstName,
    					lastName = user.lastName,
    					username = user.username,
    					rating = user.rating,
    					task_ids = user.task_ids
					});

					me.session.UserProfileModel = UserProfileModel;
					return callback(err, new me.ApiResponse(
						{ success: true, extras: { UserProfileModel:UserProfileModel } 
					}));
				} else {
					return callback(err, new me.ApiResponse(
				{ success: false, extras: { msg: me.ApiMessages.INVALID_PWD } 
			}));
				}
			});
		} else {
			return callback(err, new me.ApiResponse(
				{ success: false, extras: { msg: me.ApiMessages.EMAIL_NOT_FOUND } 
			}));
		}
	});
};

AccountController.prototype.logoff = function (){
	if (this.session.UserProfileModel) 
		delete this.session.UserProfileModel;
	return;
}


module.exports = AccountController;

