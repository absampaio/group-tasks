var express = require('express');
var router = express.Router();
var Task = require ('../models/task');

router.post('/',function(req,res){
	var t = new Task();
	t.title = req.body.title;
    t.description = req.body.description;
    t.pay_value = req.body.pay_value;
    t.group = [];
    t.creator_id = 2;

    t.save(function(err){
    	if (err){
    		res.send(err);
    	}
    	res.json ({message:'Task Created'});
    });
});

router.get('/', function (req,res){
	Task.find(function(err, tasks){
		if (err){
			res.send(err);
		}
		res.json(tasks);
	});
});

router.get('/:task_id', function (req,res){
	Task.findById(req.params.task_id,function(err,task){
		if (err) {
			res.send(err);
		}
		res.json(task);
	});
});

router.put('/:task_id', function (req,res){
	Task.findById(req.params.task_id,function(err,task){
		if (err) {
			res.send(err);
		}
		t.title = req.body.title;
    	t.description = req.body.description;
    	t.pay_value = req.body.pay_value;
    	t.group = [];
    	t.creator_id = 2;

    	t.save(function(err){
    		if (err){
    			res.send(err);
    		}
    		res.json ({message:'Task Updated'});
    	});
	});
});

router.delete('/:task_id', function (req,res){
	Task.findById(req.params.task_id,function(err,task){
		if (err) {
			res.send(err);
		}
    	res.json ({message:'Task Deleted'});
	});
});



module.exports = router;
