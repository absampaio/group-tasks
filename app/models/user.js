var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
    email: String,
    firstName: String,
    lastName: String,
    username:String,
    rating:Number,
    tasks_ids:[],
    passwordHash: String,
    passwordSalt: String
});

module.exports = mongoose.model('User', UserSchema);
