var UserProfileModel = function(cnf) {
    this.email = cnf.email,
    this.firstName = cnf.firstName,
    this.lastName = cnf.lastName,
    this.username = cnf.username,
    this.rating = cnf.rating,
    this.task_ids = cnf.task_ids
};

module.exports = UserProfileModel;
