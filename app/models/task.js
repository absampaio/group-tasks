var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var TaskSchema   = new Schema({
    title: String,
    description: String,
    pay_value: Number,
    group: [],
    creator_id: Number
});

module.exports = mongoose.model('Task', TaskSchema);